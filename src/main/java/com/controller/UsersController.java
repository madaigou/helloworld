package com.controller;

import com.entity.BaseResult;
import com.entity.Users;
import com.service.UsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/Users/")
public class UsersController extends BaseController {
    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/getUsersByPage")
    public BaseResult getUsersByPage(@RequestParam(value = "pageNum", defaultValue = "0") Integer pageNum,
                                     @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        String msg = "获取成功";
        boolean status = true;
        Page<Users> page = null;

        BaseResult baseResult = new BaseResult<Page>();
        try {
            page = usersService.findUsersByPage(pageNum, pageSize);
        } catch (Exception e) {
            msg = "获取失败";
            status = false;
        }

        baseResult.setStatus(status);
        baseResult.setMsg(msg);
        baseResult.setData(page);

        return baseResult;
    }


    @RequestMapping(value = "/login")
    @ResponseBody
    public BaseResult login(String tel, String pwd) {
        String msg = "获取成功";
        boolean status = true;
        Users users = null;

        BaseResult baseResult = new BaseResult<Users>();
        try {
            users = usersService.login(tel, pwd);
        } catch (Exception e) {
            msg = "获取失败";
            status = false;
        }

        baseResult.setStatus(status);
        baseResult.setMsg(msg);
        baseResult.setData(users);

        return baseResult;
    }
}
