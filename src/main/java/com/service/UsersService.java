package com.service;

import com.entity.Users;
import com.repository.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Created by GUOQI on 2016/12/9.
 */
@Service
public class UsersService {
    @Autowired
    private UsersRepository usersRepository;

    public void insert(Users Users) {
        usersRepository.save(Users);
    }

    public Users update(Users Users) {
        return usersRepository.saveAndFlush(Users);
    }

    public Users findUsersById(Integer id) {
        return usersRepository.findOne(id);
    }


    public Page<Users> findUsersByPage(Integer pageNum, Integer pageSize) {
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withIgnorePaths("id","name","age").withIncludeNullValues();
//        Example<Users> example = Example.of(users);

        Pageable pageable = new PageRequest(pageNum, pageSize, new Sort(Sort.Direction.ASC, "id"));
        Page<Users> page = usersRepository.findAll(pageable);
        return page;
    }

    public Users login(String tel, String pwd) {
        return usersRepository.findByTelAndPwd(tel, pwd);
    }

}
