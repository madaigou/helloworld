package com.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by GUOQI on 2016/12/12.
 */
@Configuration
@ComponentScan(basePackages = "com")
@EntityScan(basePackages = "com.entity")
public class Config {
}
