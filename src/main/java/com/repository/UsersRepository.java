package com.repository;

import com.entity.Users;

import org.springframework.stereotype.Repository;


/**
 * 实体仓库
 */
@Repository
public interface UsersRepository extends BaseRepository<Users, Integer> {

    //不需要SQL语句,这就是Spring-data-jpa的一大特性：通过解析方法名创建查询。不同表之间用"_"链接

    /**
     * 查询电话和密码
     *
     * @param tel
     * @param pwd
     * @return
     */
    Users findByTelAndPwd(String tel, String pwd);




//    Users findUsersBy();
//    Users findById(int id);

    /**
     * 除了通过解析方法名来创建查询外，它也提供通过使用@Query 注解来创建查询，
     * 您只需要编写JPQL语句，并通过类似“:name”来映射@Param指定的参数
     */
//    @Query("select from Users u where u.tel=:tel")
//    Users findPerson(@Param("tel") String tel);

}
